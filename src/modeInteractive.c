#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "../include/wordFrequencyCounter.h"

int main(int argc, char* argv[]) {

    char inputString[100]; // Supposant une longueur maximale de saisie de 99 caractères

    printf("Enter a string (write END to terminate program): ");
    fgets(inputString, sizeof(inputString), stdin);
    do {
        mode_interactive(inputString);
        printf("Enter a string (write END to terminate program): ");
        fgets(inputString, sizeof(inputString), stdin);
    } while (strcmp(inputString, "END\n") != 0);


    return 0;
}
