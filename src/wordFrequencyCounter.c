#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include "wordFrequencyCounter.h"

// Supprime le premier caractère d'une chaîne de caractères
void deleteFirstChar(char *str) {
    // Décalage de tous les caractères d'une position vers la gauche
    int len = strlen(str);
    for (int i = 0; i < len; i++) {
        str[i] = str[i+1];
    }
}

// Concatène les chaînes de caractères
void concatenate_strings(const char *strings[], int num_strings, char *result) {
    result[0] = '\0'; // Initialise la chaîne de résultat comme vide

    for (int i = 0; i < num_strings; i++) {
        strcat(result, strings[i]); // Ajoute la chaîne actuelle au résultat
        strcat(result, " ");
    }
}

// Comparer deux structures WordFrequency
int compare_word_frequency(const void *a, const void *b) {
    const WordFrequency *word1 = (const WordFrequency *)a;
    const WordFrequency *word2 = (const WordFrequency *)b;

    // Si les fréquences sont différentes, retourne la différence
    if (word1->frequency != word2->frequency) {
        return word2->frequency - word1->frequency;
    } else {
        // Sinon, compare alphabétiquement
        return strcmp(word1->word, word2->word);
    }
}

// Initialisation du compteur de fréquence
void init_frequency_counter(allUniqueWords *counter) {
    counter->words = NULL;
    counter->count = 0;
}

// Libération de la mémoire allouée pour le compteur de fréquence
void cleanup_frequency_counter(allUniqueWords *counter) {
    if (counter->words != NULL) {
        for (int i = 0; i < counter->count; ++i) {
            free(counter->words[i].word);
        }
        free(counter->words);
    }
    counter->words = NULL;
    counter->count = 0;
}

// Ajout d'un mot au compteur de fréquence
void add_word_to_counter(allUniqueWords *counter, const char *word) {
    // Vérifie si le mot existe déjà dans le compteur
    int found = 0;
    for (int i = 0; i < counter->count; ++i) {
        if (strcmp(counter->words[i].word, word) == 0) {
            // Si trouvé, incrémente la fréquence
            counter->words[i].frequency++;
            found = 1;
            break;
        }
    }
    // Si le mot n'a pas été trouvé, l'ajoute au compteur
    if (!found) {
        counter->count++;
        counter->words = realloc(counter->words, counter->count * sizeof(WordFrequency));
        if (counter->words == NULL) {
            fprintf(stderr, "Erreur d'allocation mémoire.\n");
            exit(EXIT_FAILURE);
        }
        counter->words[counter->count - 1].word = strdup(word);
        counter->words[counter->count - 1].frequency = 1;
    }
}

// Lecture du fichier et comptage des mots
void read_and_count_words(allUniqueWords *counter, const char *filename) {

    int word_count = count_words_in_file(filename);

    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        fprintf(stderr, "Erreur : Impossible d'ouvrir le fichier %s.\n", filename);
        exit(EXIT_FAILURE);
    }

    char word[MAX_WORD_LENGTH];
    int k = 0;
    while (fscanf(file, "%s", word) == 1) {
        // Convertit le mot en minuscules
        for (int i = 0; word[i]; ++i) {
            word[i] = tolower(word[i]);
        }

        // Supprime la ponctuation du mot at the beginning
        if (ispunct(word[0])) {
            deleteFirstChar(word);
        }

        // Supprime la ponctuation du mot at the end
        int len = strlen(word);
        if (ispunct(word[len - 1])) {
            word[len - 1] = '\0'; // Supprime le dernier caractère (ponctuation)
        }
        len = strlen(word);
        if (ispunct(word[len - 1])) {
            word[len - 1] = '\0'; // Supprime le dernier caractère (ponctuation)
        }

        add_word_to_counter(counter, word);

        k++;
        if (k % 1000 == 0) {
            printf("Progress: [%d/%d] \n", k, word_count);
        }
    }

    fclose(file);
}

// Tri des mots par fréquence
void sort_words_by_frequency(allUniqueWords *counter) {
    qsort(counter->words, counter->count, sizeof(WordFrequency), compare_word_frequency);
}

// Écriture des résultats dans un fichier
void write_results(const allUniqueWords *counter, const char *filename) {
    FILE *file = fopen(filename, "w");
    if (file == NULL) {
        fprintf(stderr, "Erreur : Impossible d'ouvrir le fichier %s pour écriture.\n", filename);
        exit(EXIT_FAILURE);
    }

    fprintf(file, "Word Frequency Counter\n");
    fprintf(file, "-----------------------\n");


    for (int i = 0; i < counter->count; ++i) {
        if (counter->words[i].frequency > 0){
            fprintf(file, "word: %s , frequency: %d\n", counter->words[i].word, counter->words[i].frequency);
        }
    }

    fclose(file);
}

// Écriture des résultats en excluant les mots dans un fichier
void filter_stop_words(allUniqueWords *counter, const char *filename){
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        fprintf(stderr, "Erreur : Impossible d'ouvrir le fichier %s.\n", filename);
        exit(EXIT_FAILURE);
    }

    char word[MAX_WORD_LENGTH];
    while (fscanf(file, "%s", word) == 1) {
        for (int i = 0; i < counter->count; ++i) {
            if (strcmp(counter->words[i].word, word) == 0) {
                counter->words[i].frequency = 0;
            }
        }
    }

    fclose(file);
}

// Compte les mots dans un fichier
int count_words_in_file(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        perror("Error opening file");
        return -1;
    }

    int word_count = 0;
    int in_word = 0;
    int c;

    while ((c = fgetc(file)) != EOF) {
        if (isspace(c)) {
            if (in_word) {

                in_word = 0;
            }
        } else {
            if (!in_word) {
                // Start of word
                word_count++;
                in_word = 1;
            }
        }
    }

    fclose(file);
    return word_count;
}

// Fonction principale pour compter la fréquence des mots
void word_frequency_counter(const char *input_file, const char *output_file, const char *stopWords) {
    // Création d'une instance de allUniqueWords
    allUniqueWords counter;

    // Initialisation du compteur
    init_frequency_counter(&counter);

    // Lecture du fichier d'entrée et comptage de la fréquence des mots
    read_and_count_words(&counter, input_file);

    // Filtrage des mots vides
    filter_stop_words(&counter, stopWords);

    // Tri des mots par fréquence
    sort_words_by_frequency(&counter);

    // Écriture des résultats dans le fichier de sortie
    write_results(&counter, output_file);

    // Libération de la mémoire
    cleanup_frequency_counter(&counter);

    printf("SUCCESS...\n");
}

// Mode interactif pour compter la fréquence des mots
void mode_interactive(char *input_string) {

    char input[MAX_WORD_LENGTH];
    char *words[100];
    int word_count = 0;

    strcpy(input, input_string);

    size_t length = strlen(input);
    if (length > 0 && input[length - 1] == '\n') {
        input[length - 1] = '\0'; // Remplace le caractère de nouvelle ligne par un terminateur nul
    }

   // Tokenise la chaîne d'entrée en mots
    char *token = strtok(input, " ");
    while (token != NULL && word_count < 100) {
        words[word_count++] = token;
        token = strtok(NULL, " ");
    }


    allUniqueWords counter;
    init_frequency_counter(&counter);

    for (int k = 0; k < word_count; k++) {
        // Convertit le mot en minuscules
        for (int i = 0; words[k][i]; ++i) {
            words[k][i] = tolower(words[k][i]);
        }

        // Supprime la ponctuation du mot at the beginning
        if (ispunct(words[k][0])) {
            deleteFirstChar(words[k]);
        }

        // Supprime la ponctuation du mot at the end
        int len = strlen(words[k]);
        if (ispunct(words[k][len - 1])) {
            words[k][len - 1] = '\0'; // Supprime le dernier caractère (ponctuation)
        }
        len = strlen(words[k]);
        if (ispunct(words[k][len - 1])) {
            words[k][len - 1] = '\0'; // Supprime le dernier caractère (ponctuation)
        }

        add_word_to_counter(&counter, words[k]);

    }

    sort_words_by_frequency(&counter);

    printf("Word Frequency Counter\n");
    printf("-----------------------\n");

    for (int i = 0; i < counter.count; ++i) {
        if (counter.words[i].frequency > 0){
            printf("word: %s , frequency: %d\n", counter.words[i].word, counter.words[i].frequency);
        }
    }
    cleanup_frequency_counter(&counter);

}
