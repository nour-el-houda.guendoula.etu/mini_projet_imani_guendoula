#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "../include/wordFrequencyCounter.h"

int main(int argc, char* argv[]) {

    // Récupérer les noms de fichiers
    char* input_file = argv[1];
    char* output_file = argv[2];
    char* stopWords = argv[3];

    // Appeler la fonction de comptage de fréquence des mots avec les fichiers d'entrée et de sortie spécifiés
    word_frequency_counter(input_file, output_file, stopWords);

    return 0;
}
