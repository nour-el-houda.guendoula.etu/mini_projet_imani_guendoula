#ifndef FREQUENCY_COUNTER_H
#define FREQUENCY_COUNTER_H
#define MAX_WORD_LENGTH 100
#define MAX_STRING_LENGTH 100

// Structure pour représenter un mot avec sa fréquence
typedef struct {
    char *word;   // le mot
    int frequency;  // la fréquence
} WordFrequency;

// Structure pour représenter le compteur de fréquence
typedef struct {
    WordFrequency *words;       // le tab des mots
    int count;                // le nombre de mots uniques
} allUniqueWords;

// Supprime le premier caractère d'une chaîne de caractères
void deleteFirstChar(char *str);

// Initialisation du compteur de fréquence
void init_frequency_counter(allUniqueWords *counter);

// Libération de la mémoire allouée pour le compteur de fréquence
void cleanup_frequency_counter(allUniqueWords *counter);

// Ajout d'un mot au compteur de fréquence
void add_word_to_counter(allUniqueWords *counter, const char *word);


// Lecture du fichier et comptage des mots
void read_and_count_words(allUniqueWords *counter, const char *filename);

// Tri des mots par fréquence
void sort_words_by_frequency(allUniqueWords *counter);

// Écriture des résultats dans un fichier
void write_results(const allUniqueWords *counter, const char *filename);

// Fonction de filtrage des stop words 
void filter_stop_words(allUniqueWords *counter, const char *filename);

// Compte le nombre de mots dans un fichier
int count_words_in_file(const char *filename);

// Concatène les chaînes de caractères d'un tableau en une seule chaîne
void concatenate_strings(const char *strings[], int num_strings, char *result);

// Compare la fréquence de deux mots pour le tri
int compare_word_frequency(const void *a, const void *b);

// Fonction principale pour le compteur de fréquence de mots
void word_frequency_counter(const char *input_file, const char *output_file, const char *stopWords);

// Mode interactif pour le compteur de fréquence de mots
void mode_interactive(char *input_string);

#endif /* FREQUENCY_COUNTER_H */
