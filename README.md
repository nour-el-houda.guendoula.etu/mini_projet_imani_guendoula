# Mini-Projet Test & Maintenance IS4 2024 : Word Frequency Counter
## Auteurs
- **Nour-El Houda Guendoula**
- **Fadi Imani**

## Introduction
Ce mini-projet consiste à réaliser un programme en c qui permet de compter le nombre d'occurrences de chaque mot dans un fichier texte donné. Le programme doit afficher les mots les plus fréquents et leur nombre d'occurrences.

## Fonctionnalités
1. [x] **_Insensibilité à la casse_** : Le programme doit compter les mots indépendamment de leur casse (par exemple, ?Hello? et ?hello? sont considérés comme le même mot).
2. [x] **_Traitement de la ponctuation_** : Traiter correctement la ponctuation. Les mots adjacents à la ponctuation doivent être comptés sans les signes de ponctuation (par exemple, ?bonjour,? et ?bonjour? sont identiques).
3. [x] **_Tri de fréquence_** : Le résultat doit énumérer les mots par ordre décroissant de fréquence. En cas d?égalité de fréquence, les mots sont triés par ordre alphabétique.
4. [x] **_Entrée/sortie via fichiers_** : Lire le texte source à partir d?un fichier et écrire le nombre de fréquences dans un fichier de sortie.
5. [x] **_Arguments de la ligne de commande_** : Permet à l?utilisateur de spécifier les chemins d?accès aux fichiers d?entrée et de sortie par le biais d?arguments de ligne de commande.
6. [x] **_Exclusion des mots vides (stop words)_**: Mettre en ?uvre une fonctionnalité permettant d?exclure les mots vides courants (comme ?the?, ?is?, ?at?, ?which?, etc.) du comptage. ). Une liste par défaut est fournie (voir annexe), mais l?utilisateur peut également spécifier un fichier de mots vides personnalisé.
7. [x] **_Mise à jour de la progression en temps réel_** : Pour les fichiers volumineux, affichage d?un indicateur de progression indiquant la part du fichier qui a été traitée.
8. [x] **_Mode interactif_** : Ajout d?un mode interactif dans lequel l?utilisateur peut saisir un texte directement et obtenir l?analyse de la fréquence en temps réel.

## Prérequis
```bash
  - apt-get update -qq
  - apt-get install -y cmake
  - apt-get install -y lcov
```

## Compilation du programme
```bash
    - mkdir build
    - cd build
    - cmake ..
    - make -j8
    - cp ../tests/*.txt .
```


## Exécution
Pour lancer les testes unitaires, il suffit de lancer la commande suivante :
```bash
    - ./bin/ExampleTests
```
Pour lancer le mode interactif, il suffit de lancer la commande suivante :
```bash
    - ./bin/modeInteractif
```
Pour lancer le programme avec un fichier texte, il suffit de lancer la commande suivante :
```bash
    - ./bin/count_words_CLI_args frankenstein.txt output2.txt stopWords.txt
```

## CI/CD Pipeline
Le pipeline CI/CD a été réalisé avec succès.

![img_2.png](img_2.png)
![img_3.png](img_3.png)

## Couverture de code
La couverture de code a été réalisée avec succès.

![img_4.png](img_4.png)

## Exemple
Les tests unitaires ont été réalisés avec succès.

![img.png](img.png)

Le mode interactif a été testé avec succès.

![img_1.png](img_1.png)

## Conclusion

À travers ce mini-projet, nous avons eu l'opportunité de mettre en pratique les TDD, une approche de développement où nous écrivons les tests unitaires avant le code de production proprement dit. Cela nous a permis de comprendre pleinement l'importance des tests dans le processus de développement logiciel, garantissant que chaque fonctionnalité ajoutée était accompagnée de tests automatisés rigoureux.




