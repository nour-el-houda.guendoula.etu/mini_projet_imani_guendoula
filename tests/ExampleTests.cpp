#include "gtest/gtest.h"

// Ceci montre comment inclure notre code C dans le test
extern "C" {
    #include "../include/wordFrequencyCounter.h"
}

// Test de la fonction deleteFirstChar
TEST(ExampleTests, test_deleteFirstChar) {
    char str[] = "hello";
    deleteFirstChar(str);
    EXPECT_STREQ("ello", str);
}

// Test de la fonction init_frequency_counter
TEST(ExampleTests, test_init_frequency_counter) {
    allUniqueWords counter;
    init_frequency_counter(&counter);
    EXPECT_EQ(0, counter.count);
    EXPECT_TRUE(counter.words==NULL);
}

// Test de la fonction add_word_to_counter
TEST(ExampleTests, test_add_word_to_counter) {
    allUniqueWords counter;
    init_frequency_counter(&counter);
    add_word_to_counter(&counter, "hello");
    EXPECT_EQ(1, counter.count);
    EXPECT_STREQ("hello", counter.words[0].word);
    EXPECT_EQ(1, counter.words[0].frequency);
    add_word_to_counter(&counter, "hello");
    EXPECT_EQ(1, counter.count);
    EXPECT_STREQ("hello", counter.words[0].word);
    EXPECT_EQ(2, counter.words[0].frequency);
    add_word_to_counter(&counter, "world");
    EXPECT_EQ(2, counter.count);
    EXPECT_STREQ("world", counter.words[1].word);
    EXPECT_EQ(1, counter.words[1].frequency);
    cleanup_frequency_counter(&counter);
}

// Test de la fonction cleanup_frequency_counter
TEST(ExampleTests, test_cleanup_frequency_counter) {
    allUniqueWords counter;
    init_frequency_counter(&counter);
    add_word_to_counter(&counter, "hello");
    add_word_to_counter(&counter, "hello");
    add_word_to_counter(&counter, "World");
    cleanup_frequency_counter(&counter);
    EXPECT_EQ(0, counter.count);
    EXPECT_TRUE(counter.words==NULL);
}

// Test de la fonction count_words_in_file
TEST(ExampleTests, test_count_words_in_file) {
    EXPECT_EQ(6, count_words_in_file("test.txt"));
}

// Test de la fonction read_and_count_words
TEST(ExampleTests, test_read_and_count_words) {
    allUniqueWords counter;
    init_frequency_counter(&counter);
    char cwd[1024];
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        printf("Current working directory: %s\n", cwd);
    }
    read_and_count_words(&counter, "test.txt");
    EXPECT_EQ(3, counter.count);
    EXPECT_STREQ("hello", counter.words[0].word);
    EXPECT_EQ(3, counter.words[0].frequency);
    EXPECT_STREQ("world", counter.words[1].word);
    EXPECT_EQ(1, counter.words[1].frequency);
    EXPECT_STREQ("foo", counter.words[2].word);
    EXPECT_EQ(2, counter.words[2].frequency);
    cleanup_frequency_counter(&counter);
}

// Test de la fonction read_and_count_words avec insensibilité à la casse
TEST(ExampleTests, test_read_and_count_words_case_insensitive) {
    allUniqueWords counter;
    init_frequency_counter(&counter);
    char cwd[1024];
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        printf("Current working directory: %s\n", cwd);
    }
    read_and_count_words(&counter, "test.txt");
    EXPECT_EQ(3, counter.count);
    EXPECT_STREQ("hello", counter.words[0].word);
    EXPECT_EQ(3, counter.words[0].frequency);
    EXPECT_STREQ("world", counter.words[1].word);
    EXPECT_EQ(1, counter.words[1].frequency);
    EXPECT_STREQ("foo", counter.words[2].word);
    EXPECT_EQ(2, counter.words[2].frequency);
    cleanup_frequency_counter(&counter);
}

// Test de la fonction read_and_count_words avec ponctuation
TEST(ExampleTests, test_read_and_count_words_ponctuation) {
    allUniqueWords counter;
    init_frequency_counter(&counter);
    char cwd[1024];
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        printf("Current working directory: %s\n", cwd);
    }
    read_and_count_words(&counter, "test.txt");
    EXPECT_EQ(3, counter.count);
    EXPECT_STREQ("hello", counter.words[0].word);
    EXPECT_EQ(3, counter.words[0].frequency);
    EXPECT_STREQ("world", counter.words[1].word);
    EXPECT_EQ(1, counter.words[1].frequency);
    EXPECT_STREQ("foo", counter.words[2].word);
    EXPECT_EQ(2, counter.words[2].frequency);
    cleanup_frequency_counter(&counter);
}

// Test de la fonction compare_word_frequency
TEST(ExampleTests, test_compare_word_frequency) {
    WordFrequency a = {"hello", 3};
    WordFrequency b = {"world", 1};
    EXPECT_TRUE(compare_word_frequency(&a, &b) < 0);
}

// Test de la fonction compare_word_frequency lorsque les fréquences sont égales
TEST(ExampleTests, test_compare_word_frequency_when_equal) {
    WordFrequency a = {"hello", 3};
    WordFrequency b = {"world", 3};
    EXPECT_TRUE(compare_word_frequency(&a, &b) < 0);
}

// Test de la fonction sort_words_by_frequency
TEST(ExampleTests, test_sort_words_by_frequency) {
    allUniqueWords counter;
    init_frequency_counter(&counter);
    read_and_count_words(&counter, "test.txt");
    sort_words_by_frequency(&counter);
    EXPECT_EQ(3, counter.count);
    EXPECT_STREQ("hello", counter.words[0].word);
    EXPECT_STREQ("foo", counter.words[1].word);
    EXPECT_STREQ("world", counter.words[2].word);
    cleanup_frequency_counter(&counter);
}

// Test de la fonction sort_words_by_frequency lorsque les fréquences sont égales
TEST(ExampleTests, test_sort_words_by_frequency_when_equal) {
    allUniqueWords counter;
    init_frequency_counter(&counter);
    add_word_to_counter(&counter, "hello");
    add_word_to_counter(&counter, "hello");
    add_word_to_counter(&counter, "world");
    add_word_to_counter(&counter, "world");
    sort_words_by_frequency(&counter);
    EXPECT_EQ(2, counter.count);
    EXPECT_STREQ("hello", counter.words[0].word);
    EXPECT_STREQ("world", counter.words[1].word);
    cleanup_frequency_counter(&counter);
}

// Test de la fonction write_results
TEST(ExampleTests, test_write_results) {
    allUniqueWords counter;
    init_frequency_counter(&counter);
    read_and_count_words(&counter, "test.txt");
    sort_words_by_frequency(&counter);
    write_results(&counter, "output.txt");
    cleanup_frequency_counter(&counter);
    // check the output file
    FILE *f = fopen("output.txt", "r");
    char line[100];
    fgets(line, 100, f);
    fgets(line, 100, f);
    fgets(line, 100, f);
    EXPECT_STREQ("word: hello , frequency: 3\n", line);
    fgets(line, 100, f);
    EXPECT_STREQ("word: foo , frequency: 2\n", line);
    fgets(line, 100, f);
    EXPECT_STREQ("word: world , frequency: 1\n", line);
    fclose(f);
}

// Test de la fonction filter_stop_words
TEST(ExampleTests, test_filter_stop_words) {
    allUniqueWords counter;
    init_frequency_counter(&counter);
    read_and_count_words(&counter, "test.txt");
    sort_words_by_frequency(&counter);
    filter_stop_words(&counter, "stopWords.txt");
    write_results(&counter, "output.txt");
    EXPECT_EQ(3, counter.count);
    EXPECT_STREQ("hello", counter.words[0].word);
    EXPECT_EQ(3, counter.words[0].frequency);
    EXPECT_STREQ("foo", counter.words[1].word);
    EXPECT_EQ(2, counter.words[1].frequency);
    cleanup_frequency_counter(&counter);
}

// Test de la fonction concatenate_strings
TEST(ExampleTests, test_concatenate_strings) {
    const char *arr[] = {"This", "is", "an", "array", "of", "strings"};
    int num_strings = sizeof(arr) / sizeof(arr[0]);
    char result[MAX_STRING_LENGTH] = "";

    concatenate_strings(arr, num_strings, result);
    EXPECT_STREQ("This is an array of strings ", result);
}

// Test de la fonction word_frequency_counter
TEST(ExampleTests, test_word_frequency_counter) {
    word_frequency_counter("test.txt", "output.txt", "stopWords.txt");
    // check the output file
    FILE *f = fopen("output.txt", "r");
    char line[100];
    fgets(line, 100, f);
    fgets(line, 100, f);
    fgets(line, 100, f);
    EXPECT_STREQ("word: hello , frequency: 3\n", line);
    fgets(line, 100, f);
    EXPECT_STREQ("word: foo , frequency: 2\n", line);
    fclose(f);
}

// Test de la fonctionnalité de la barre de progression
TEST(ExampleTests, test_progress_bar){
    allUniqueWords counter;
    init_frequency_counter(&counter);
    read_and_count_words(&counter, "frankenstein.txt");
}

// Test de la fonctionnalité du mode interactif
TEST(ExampleTests, test_mode_interactive) {
    mode_interactive("my name is fadi fadi");
}




// executer les tests
int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
